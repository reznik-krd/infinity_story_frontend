import './style/mui.css' //внешний
import './style/main.css'
import './style/panel.css'
import './style/scrollbar.css'
import './style/utils.css'
import './style/visualization.css'

import {fillBoxPage} from "./navigation";

export const domenUrl = 'http://localhost:8081' //localhost:8081 https://infinity-story-backend.herokuapp.com - на какой бэк смотреть

fillBoxPage() //заполнение контента при обновлении страницы согласно hash в url

$(function () {
    $(document).on("click", '.switch-sidedrawer-rrr', () => {
        if (document.body.classList.contains('hide-sidedrawer')) {
            document.body.classList.remove('hide-sidedrawer')
        } else {
            document.body.classList.add('hide-sidedrawer')
        }
    });
});