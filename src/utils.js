//проверяет значение, иначе выдает пустую строку
export function getValueOrEmpty(value) {
    return (value === undefined || value == null) ? "" : value
}

//обёртка, проверяет сетевой ответ от бэка
export function status(response) {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
    } else {
        return Promise.reject(new Error(response.statusText))
    }
}

//обёртка, возвращает json из ответа
export function json(response) {
    return response.json()
}

export function getPanelRight() {
    return `
        <div class="side-panel">
            <div class="panel-box">
                <div class="panel-box-inner-left">
                    <strong> Список людей </strong> 
                </div>
                <label class="panel-box-inner-right side-button-close">+</label>
            </div>
            <br>
            <div style="margin-top: 30px">
                <div class="split"> 
                   <div id="js-personalCards"></div>
                </div>
            </div>
        </div>
    `
}