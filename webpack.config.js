const path = require('path') //вызывает системные команды винды для работы с путями
const webpack = require('webpack'); //запрос вебпака
const HTMLPlugin = require('html-webpack-plugin') //позволяет на основе шаблона генерировать html с уже подключенным скриптом
const {CleanWebpackPlugin} = require('clean-webpack-plugin') //для чистки билдов со старыми хэш названиями

//конфигурационный модуль для webpack
module.exports = {
    entry: './src/app.js', //тут путь, с чего всё начинается
    output: { //во что в итоге webpack превратит наш проект
        filename: 'bundle.[chunkhash].js', //название, chunkhash - генерит новые названия билдов js
        path: path.resolve(__dirname, 'public') //куда складываем соединенные js скрипты, __dirname - указываем что работаем от текущей директории
    },
    devServer: { //настройка для сервера разработки, для автоматической перезагрузки и отладки
        port: 3000 //порт указываем
    },
    //указание и создание инстанцев плагинов, которые мы подключаем как модули
    plugins: [
        //прописываем использование плагина в конкретный html (главный)
        new HTMLPlugin({
            template: './src/index.html'
        }),
        new CleanWebpackPlugin(),
        new webpack.ProvidePlugin({
            "$":"jquery",
            "jQuery":"jquery",
            "window.jQuery":"jquery"
        })
    ],
    resolve : {
        alias: {
            'jquery-ui': 'jquery-ui-dist/jquery-ui.js'
        }
    },
    module: {
        rules: [
            {
                //webpack изначально умеет работать только с js, чтобы подключить стили, нужно добавить это
                test: /\.css$/i, //расширение css
                use: ["style-loader", "css-loader"], //прогоняет файлы через стили
            },
        ],
    }
}